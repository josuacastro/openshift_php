<?php
ob_start();
require_once ('config.php'); 
session_start();
?>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/signin.css">
    <title>Iniciar Sesion</title>
</head>
<body>
<div class="container">
    <form action="" method="post" name="Login_Form" class="form-signin">
        <h2 class="form-signin-heading">Inicie Sesion</h2>
        <label for="inputUsername" class="sr-only">Usuario</label>
        <input name="Username" type="username" id="inputUsername" class="form-control" placeholder="Username" required autofocus>
        <label for="inputPassword" class="sr-only">Contraseña</label>
        <input name="Password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <div class="checkbox">
            <label>
                <input type="checkbox" value="remember-me"> Recuerdame
            </label>
        </div>
        <button name="Submit" value="Login" class="btn btn-lg btn-primary btn-block" type="submit">Iniciar Sesion</button>

        <?php
        
        if(isset($_POST['Submit'])){
            
            $result = password_verify($_POST['Password'], $Password);
            
            if( ($_POST['Username'] == $Username) && ($result === true) ) {
                
                $_SESSION['Username'] = $Username;
                $_SESSION['Active'] = true;
                header("location:index.php");
                exit;
            } else {
                ?>
                
                &nbsp;
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Warning!</strong> Informacion Incorr.
                </div>
                <?php
            }
        }
        ?>

    </form>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script src="js/bootstrap.min.js"></script>
</body>
</html>