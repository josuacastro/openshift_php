<?php

  session_start(); 

  if($_SESSION['Active'] == false){ 
    header("location:login.php");
	  exit;
  }
?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/stylesheet.css">
    <title>Iniciar</title>
  </head>
  <body>
    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="#">Inicio</a></li>
            <li role="presentation"><a href="#">Nosotros</a></li>
            <li role="presentation"><a href="#">Contacto</a></li>
          </ul>
        </nav>
        <h3 class="text-muted">Sesion Final - Curso Openshift OKD</h3>
      </div>

      <div class="jumbotron">
        <h1>Estado : Conectado</h1>
        <p class="lead">Y así, ha creado su primera área protegida por contraseña con PHP y un poco de conocimiento de HTML.</p>
        <p><a class="btn btn-lg btn-success" href="logout.php" role="button">Log out</a></p>
      </div>

      <div class="row marketing">

        <div class="col-lg-6">
          <h4>Os-Enterprise</h4>
          <p>Docker</p>
          <p>Kubernetes</p>
          <p>Jenkins</p>
          <p>REDIS</p>
        </div>
      </div>

      <footer class="footer">
        <p>&copy; Josua Castro</p>
      </footer>

    </div>

    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
